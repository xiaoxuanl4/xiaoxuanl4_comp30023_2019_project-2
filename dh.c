/*socket programming part comes from comp30023 lab5 sample code.
  credit to the person who write that sample code*/


/* A simple client program for server.c

   To compile: gcc client.c -o client

   To run: start the server, then the client */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <ctype.h>

// Function to compute a^m mod n, comes from www.techiedelight.com
int compute(int a, int m, int n)
{
	int r;
	int y = 1;

	while (m > 0)
	{
		r = m % 2;

		// fast exponention
		if (r == 1)
			y = (y*a) % n;
		a = a*a % n;

		m = m / 2;
	}

	return y;
}

int main(int argc, char ** argv)
{
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent * server;

    char buffer[256];

    portno = 7800;


    /* Translate host name into peer's IP address ;
     * This is name translation service by the operating system
     */
    server = gethostbyname("172.26.37.44");

    if (server == NULL)
    {
        fprintf(stderr, "ERROR, no such host\n");
        exit(0);
    }

    /* Building data structures for socket */

    bzero((char *)&serv_addr, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;

    bcopy(server->h_addr_list[0], (char *)&serv_addr.sin_addr.s_addr, server->h_length);

    serv_addr.sin_port = htons(portno);

    /* Create TCP socket -- active open
    * Preliminary steps: Setup: creation of active open socket
    */

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0)
    {
        perror("ERROR opening socket");
        exit(0);
    }

    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("ERROR connecting");
        exit(0);
    }

    /* Do processing
    */

    //send user name

    bzero(buffer, 256);

    sprintf(buffer, "xiaoxuanl4\n");
    n = write(sockfd, buffer, strlen(buffer));
    printf("%s\n", buffer);

    //send g^b(mod p)

    int g=15;
    int p=97;

    bzero(buffer, 256);

    char hash[256];
    FILE* file = popen("openssl sha256 dh.c", "r");
    if(!file){
        printf("something wrong with your command or file\n");
        exit(0);
    }

    while(fgets(hash, 256, file)){
        strcat(buffer, hash);
        bzero(hash, 256);
    }
    pclose(file);
    printf("%s\n", buffer);

    char res[256];

    char* temp = malloc(3*sizeof(char*));

    temp = strstr(buffer, "SHA256(dh.c)= ") + 14;
    temp[2] = '\0';
    printf("%s\n", temp);
    strcpy(res, temp);

    temp = NULL;
    free(temp);

    int b1 = 0;
    int b2 = 0;
    int b = 0;


    if(isalpha(res[0])) {
		//fix with alpha case
		 b1 = res[0] - 87;
  	} else {
  		//fix with integer case
  		b1 = res[0] - 48;
  	}
    if(isalpha(res[1])) {
		//fix with alpha case
		 b2 = res[1] - 87;
  	} else {
  		//fix with integer case
  		b2 = res[1] - 48;
  	}

    b = (b1*16)+b2;

    printf("%d\n", b);

    bzero(buffer, 256);
    bzero(res, 256);

    //Assume I am Bob
  	int Bob_req = compute(g, b, p);

  	sprintf(buffer, "%d\n", Bob_req);
    n = write(sockfd, buffer, strlen(buffer));
    printf("%s\n", buffer);
    if (n < 0)
    {
        perror("ERROR writing to socket");
        exit(0);
    }

    bzero(buffer, 256);

    n = read(sockfd, buffer, 255);
    printf("%s\n", buffer);
    if (n < 0)
    {
        perror("ERROR reading from socket");
        exit(0);
    }
    //server is Alice
  	int Alice_res = atoi(buffer);
  	int Bob_res_again= compute(Alice_res, b, p);

    bzero(buffer, 256);
  	sprintf(buffer, "%d\n", Bob_res_again);
  	n = write(sockfd, buffer, strlen(buffer));
    printf("%s\n", buffer);
  	bzero(buffer, 256);

    n = read(sockfd, buffer, 255);

    printf("status report: %s\n", buffer);

    return 0;
}
