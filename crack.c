/***********************************************************
* Author: Xiaoxuan Li 933206
***********************************************************/

#include <stdio.h>
#include <memory.h>
#include <string.h>
#include <stdbool.h>
#include "sha256.c"

#define SHA256_BLOCK_SIZE 32
#define PASSWORD_NUM 20
#define PWD4SHA256_LEN 320
#define MAX_WORD_LEN 10000 //comes from discussion forum
#define START 32//given in specification
#define END 127
#define A_START 97 //ascii value for alphabets
#define A_END 124
#define I_START 48//ascii value for integers
#define I_END 58



/***************function declarations**********************/
bool compare();
/**********************************************************/



int main(int argc, char* argv[]){

  /***************no arguements provided scenario*****************/
  if(argc == 1){
      unsigned char target[PASSWORD_NUM][SHA256_BLOCK_SIZE];
      FILE* pwd6sha256 = fopen("pwd6sha256", "rb");
      for (int i = 0; i < PASSWORD_NUM; i++){
          fread(target[i], sizeof(target[i]), 1, pwd6sha256);
      }

      fseek(pwd6sha256, 0, SEEK_END);
      int pwd6sha256_size;
      pwd6sha256_size = ftell(pwd6sha256);
      fseek(pwd6sha256, 0, SEEK_SET);

      char test_word[10];

      char guess[10];
      FILE* common_passwords;
      common_passwords = fopen("generated_common_pwd.txt", "r");
      int dic_count;
      dic_count = 6836;

      while(dic_count){
        fgets(guess, 10, common_passwords);
        guess[6]='\0';
        compare(guess, target, pwd6sha256_size);
        dic_count--;
      }


      //Brute force for 6 char pwds.
      for(int i = I_START; i < I_END; i++){
        for(int j = I_START; j < I_END; j++){
          for(int k = I_START; k < I_END; k++){
            for(int l = I_START; l < I_END; l++){
              for(int m = I_START; m < I_END; m++){
                for(int n = I_START; n < I_END; n++){
                  test_word[0] = i;
                  test_word[1] = j;
                  test_word[2] = k;
                  test_word[3] = l;
                  test_word[4] = m;
                  test_word[5] = n;
                  test_word[6] = '\0';
                  compare(test_word, target,pwd6sha256_size);
                }
              }
            }
          }
        }
      }

      for(int i = A_START; i < A_END; i++){
        for(int j = A_START; j < A_END; j++){
          for(int k = A_START; k < A_END; k++){
            for(int l = A_START; l < A_END; l++){
              for(int m = A_START; m < A_END; m++){
                for(int n = A_START; n < A_END; n++){
                  test_word[0] = i;
                  test_word[1] = j;
                  test_word[2] = k;
                  test_word[3] = l;
                  test_word[4] = m;
                  test_word[5] = n;
                  test_word[6] = '\0';
                  compare(test_word, target, pwd6sha256_size);
                }
              }
            }
          }
        }
      }
      fclose(pwd6sha256);
    }

  /*********************one arguement scenario*********************/
  /*
  the dictionary "generated_common_pwd.txt" used in this mode
  is generated by the author Xiaoxuan Li.

 "generated_common_pwd.txt" contains passwords in "common_passwords.txt" and
 the six characters passwords shared by other students of this subjects.

 All duplicated words are removed. Passwords those length are greater than 6
 are cutted and their first six characters are used.
  */

  if( argc == 2){
    int count = atoi(argv[1]);
    char guess[10];
    FILE* common_passwords;
    common_passwords = fopen("generated_common_pwd.txt", "r");

    //the dictionary file contains 6836 entries.
    if(count < 6836){
      while(count){
        fgets(guess, 10, common_passwords);
        guess[6]='\0';
        printf("%s\n", guess);
        count--;
      }

      fclose(common_passwords);
    }

    //dictionary exhausted
    else{
      int dic_count;
      dic_count = 6836;

      //print words in dictionary first
      while(dic_count){
        fgets(guess, 10, common_passwords);
        guess[6]='\0';
        printf("%s\n", guess);
        dic_count--;
        count--;
      }

      char test_word[10];
      //generate lower case alphabets combinaiton guesses
        for(int i = A_START; i < A_END; i++){
          for(int j = A_START; j < A_END; j++){
            for(int k = A_START; k < A_END; k++){
              for(int l = A_START; l < A_END; l++){
                for(int m = A_START; m < A_END; m++){
                  for(int n = A_START; n < A_END; n++){
                    test_word[0] = i;
                    test_word[1] = j;
                    test_word[2] = k;
                    test_word[3] = l;
                    test_word[4] = m;
                    test_word[5] = n;
                    test_word[6] = '\0';
                    printf("%s\n", test_word);
                    count--;
                    if(count<0){
                      exit(0);
                    }
                  }
                }
              }
            }
          }
        }
      fclose(common_passwords);
    }

  }

  /*********************two arguements scenarios*********************/
  if( argc == 3){
    FILE* test_file;
    FILE* target_file;
    test_file = fopen(argv[1],"r");
    target_file = fopen(argv[2], "rb");

    //get the length of target file
    int target_size;
		fseek(target_file, 0, SEEK_END);
    target_size=ftell(target_file);
    fseek(target_file, 0, SEEK_SET);

    //get password numbers
    int pwd_num;
    pwd_num = target_size/SHA256_BLOCK_SIZE;

    unsigned char target_harshes[1000][SHA256_BLOCK_SIZE];
    for(int i = 0; i < pwd_num; i ++){
      fread(target_harshes[i], sizeof(target_harshes[i]), 1, target_file);
    }

    char guess[MAX_WORD_LEN];
    int count = 10000;
    while(count){
      int i = 0;
      int c;

    	while(((c = fgetc(test_file)) != EOF) && (c !='\n') && (c != '\r')) {
    		guess[i++] = c;
    	}
    	guess[i] = '\0';
      compare(guess, target_harshes, target_size);
      count--;
    }

    fclose(test_file);
    fclose(target_file);
 }

  return 0;

}

bool compare(char test_word[], unsigned char target[][SHA256_BLOCK_SIZE],
  int size){
  //hash our test word
  SHA256_CTX ctx;
  BYTE hash[SHA256_BLOCK_SIZE];

  sha256_init(&ctx);
  sha256_update(&ctx, (BYTE*) test_word, strlen(test_word));
  sha256_final(&ctx, hash);

  int password_num;
  password_num = (size/32) + 1;

  // Compare the hash to the hash of target words
  for (int i = 0; i < password_num; i++){
      int matched_block_num = 0;
      for (int j = 0; j < SHA256_BLOCK_SIZE; j++){
          if (target[i][j] == hash[j]){
              matched_block_num++;
          }
      }
      // password found
      if (matched_block_num == SHA256_BLOCK_SIZE){
          printf("%s %d\n", test_word, i+1);
          return true;
      }
  }
  return false;
}
